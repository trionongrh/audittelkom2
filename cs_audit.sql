-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 20, 2018 at 01:18 PM
-- Server version: 10.1.30-MariaDB
-- PHP Version: 7.2.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cs_audit`
--

-- --------------------------------------------------------

--
-- Table structure for table `acara`
--

CREATE TABLE `acara` (
  `id_acara` int(11) NOT NULL,
  `nama_acara` varchar(100) NOT NULL,
  `id_iso` int(11) NOT NULL,
  `tujuan` text NOT NULL,
  `tanggal_mulai` date NOT NULL,
  `tanggal_selesai` date NOT NULL,
  `status` enum('belum_mulai','sedang_berjalan','selesai') NOT NULL,
  `pertanyaan` enum('0','1') NOT NULL,
  `publish` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `acara`
--

INSERT INTO `acara` (`id_acara`, `nama_acara`, `id_iso`, `tujuan`, `tanggal_mulai`, `tanggal_selesai`, `status`, `pertanyaan`, `publish`) VALUES
(2, 'aaaaaaa', 1, 'bbbbbbbbbbbbb', '2018-12-09', '2018-12-16', 'selesai', '1', '0'),
(3, 'asdaasd', 2, 'nnnnnnnnnnnnn', '2018-12-11', '2018-12-29', 'sedang_berjalan', '1', '1'),
(4, 'jjjjjjjjjjjj', 1, 'kkkkkkkkkkkkk', '2018-12-08', '2018-12-15', 'selesai', '0', '0'),
(5, 'Acara pertama', 2, 'tes doang', '2018-12-10', '2018-12-11', 'selesai', '0', '0'),
(6, 'Budi', 1, 'Untuk mengambil data test', '2018-12-16', '2018-12-20', 'sedang_berjalan', '1', '1'),
(7, 'Pemeliharaan Lingkungan Kampus', 1, 'Agar kampus tetap terjaga dan tidak adanya mahasiswa yang melanggar aturan kampus', '2018-12-20', '2018-12-28', 'sedang_berjalan', '0', '0'),
(8, 'Test 2', 1, 'ori', '2018-12-19', '2018-12-27', 'sedang_berjalan', '0', '0');

-- --------------------------------------------------------

--
-- Table structure for table `auditee`
--

CREATE TABLE `auditee` (
  `id_auditee` int(225) NOT NULL,
  `id_kantor` int(225) NOT NULL,
  `id_direktur` int(225) NOT NULL,
  `id_bagian` int(225) NOT NULL,
  `id_urusan` int(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `bagian`
--

CREATE TABLE `bagian` (
  `id_bagian` int(255) NOT NULL,
  `nama_bagian` varchar(255) NOT NULL,
  `id_direktorat` int(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `bagian`
--

INSERT INTO `bagian` (`id_bagian`, `nama_bagian`, `id_direktorat`) VALUES
(1, 'aaaaaaaaaaaaaa', 1),
(2, 'keuangan', 1),
(3, 'sekertariat', 5);

-- --------------------------------------------------------

--
-- Table structure for table `direktorat`
--

CREATE TABLE `direktorat` (
  `id_direktorat` int(255) NOT NULL,
  `nama_direktorat` varchar(255) NOT NULL,
  `id_kantor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `direktorat`
--

INSERT INTO `direktorat` (`id_direktorat`, `nama_direktorat`, `id_kantor`) VALUES
(1, 'Triono Nugroho', 3),
(2, 'Bardian Aditya', 3),
(3, 'ab', 7),
(4, 'cd', 7),
(5, 'sdsds', 6);

-- --------------------------------------------------------

--
-- Table structure for table `hasil`
--

CREATE TABLE `hasil` (
  `id_hasil` int(11) NOT NULL,
  `id_auditee` int(255) NOT NULL,
  `id_pertanyaan` int(255) NOT NULL,
  `id_jawaban` int(255) NOT NULL,
  `presentase` decimal(65,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hasil`
--

INSERT INTO `hasil` (`id_hasil`, `id_auditee`, `id_pertanyaan`, `id_jawaban`, `presentase`) VALUES
(1, 1, 1, 1, '50'),
(2, 2, 2, 2, '30'),
(3, 3, 3, 3, '80'),
(4, 4, 4, 4, '20');

-- --------------------------------------------------------

--
-- Table structure for table `iso`
--

CREATE TABLE `iso` (
  `id_iso` int(11) NOT NULL,
  `nama_iso` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `iso`
--

INSERT INTO `iso` (`id_iso`, `nama_iso`) VALUES
(1, 'ISO 9001'),
(2, 'ISO 20000'),
(3, 'ISO 1000');

-- --------------------------------------------------------

--
-- Table structure for table `jawaban`
--

CREATE TABLE `jawaban` (
  `id_jawaban` int(225) NOT NULL,
  `jawaban` text NOT NULL,
  `id_pertanyaan` int(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `kantor`
--

CREATE TABLE `kantor` (
  `id_kantor` int(225) NOT NULL,
  `nama_kantor` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kantor`
--

INSERT INTO `kantor` (`id_kantor`, `nama_kantor`) VALUES
(1, 'WR3'),
(2, 'WR2'),
(3, 'WR1'),
(4, 'WR4'),
(6, 'WR5'),
(7, 'WR6');

-- --------------------------------------------------------

--
-- Table structure for table `klausul`
--

CREATE TABLE `klausul` (
  `id_klausul` int(11) NOT NULL,
  `kode_klausul` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `id_iso` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `klausul`
--

INSERT INTO `klausul` (`id_klausul`, `kode_klausul`, `deskripsi`, `id_iso`) VALUES
(1, '4.1', 'a', 1),
(2, '4.2', 'empat koma 2', 3),
(3, '4.3', 'empat koma tigaaa', 3),
(5, '4.7', 'empat koma tujuh', 2),
(6, '4.2', 'asd', 1),
(7, '4.3', 'ees', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_session`
--

CREATE TABLE `login_session` (
  `uid` bigint(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(255) NOT NULL,
  `level` enum('admin','auditor','auditee') NOT NULL,
  `sotk` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_session`
--

INSERT INTO `login_session` (`uid`, `username`, `password`, `level`, `sotk`) VALUES
(1, 'admin', 'admin', 'admin', ''),
(2, 'auditor', 'auditor', 'auditor', ''),
(3, 'auditee', 'auditee', 'auditee', ''),
(4, 'admin2', 'admin2', 'admin', NULL),
(5, 'kantor1', 'kantor1', 'auditee', 'kantor_3'),
(6, 'kantor2', 'kantor2', 'auditee', 'kantor_2'),
(7, 'kantor3', 'kantor3', 'auditee', 'kantor_1'),
(8, 'kantor4', 'kantor4', 'auditee', 'kantor_4'),
(9, 'bagian1', 'bagian1', 'auditee', 'bagian_1');

-- --------------------------------------------------------

--
-- Table structure for table `pertanyaan`
--

CREATE TABLE `pertanyaan` (
  `id_pertanyaan` int(225) NOT NULL,
  `pertanyaan` text NOT NULL,
  `id_acara` int(11) NOT NULL,
  `id_klausul` int(11) NOT NULL,
  `target_auditee` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pertanyaan`
--

INSERT INTO `pertanyaan` (`id_pertanyaan`, `pertanyaan`, `id_acara`, `id_klausul`, `target_auditee`) VALUES
(1, '[\"Apa urusan anda menanyakan hal itu ?\",\"Apa urusan anda menanyakan hal itu ? (2)\"]', 6, 1, '[\"kantor_2\",\"direktorat_1\",\"bagian_1\"]'),
(2, '[\"Kenapa saya jago sekali ?\",\"Siapakah developer web ini ?\",\"Berapakah saya dibayar ?\"]', 6, 6, '[\"direktorat_4\",\"bagian_1\",\"bagian_2\"]'),
(3, '[\"Ono ganteng, paling ganteng\"]', 6, 7, '[\"kantor_3\",\"direktorat_5\",\"bagian_1\"]'),
(4, '[\"ee\"]', 3, 5, '[\"kantor_2\",\"kantor_4\",\"kantor_7\",\"direktorat_1\",\"bagian_1\"]'),
(5, '[\"Siapakah andi ?\"]', 2, 1, '[\"kantor_4\",\"direktorat_1\",\"direktorat_5\",\"bagian_2\"]'),
(6, '[\"budi\"]', 2, 6, '[\"kantor_4\",\"kantor_7\"]'),
(7, '[\"endah\",\"safira\",\"putri\"]', 2, 7, '[\"kantor_2\",\"kantor_4\"]');

-- --------------------------------------------------------

--
-- Table structure for table `urusan`
--

CREATE TABLE `urusan` (
  `id_urusan` int(225) NOT NULL,
  `nama_urusan` text NOT NULL,
  `id_bagian` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `urusan`
--

INSERT INTO `urusan` (`id_urusan`, `nama_urusan`, `id_bagian`) VALUES
(1, 'ttttttttttttt', 1),
(2, 'vvvvvvv', 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acara`
--
ALTER TABLE `acara`
  ADD PRIMARY KEY (`id_acara`);

--
-- Indexes for table `auditee`
--
ALTER TABLE `auditee`
  ADD PRIMARY KEY (`id_auditee`);

--
-- Indexes for table `bagian`
--
ALTER TABLE `bagian`
  ADD PRIMARY KEY (`id_bagian`);

--
-- Indexes for table `direktorat`
--
ALTER TABLE `direktorat`
  ADD PRIMARY KEY (`id_direktorat`);

--
-- Indexes for table `hasil`
--
ALTER TABLE `hasil`
  ADD PRIMARY KEY (`id_hasil`);

--
-- Indexes for table `iso`
--
ALTER TABLE `iso`
  ADD PRIMARY KEY (`id_iso`);

--
-- Indexes for table `jawaban`
--
ALTER TABLE `jawaban`
  ADD PRIMARY KEY (`id_jawaban`);

--
-- Indexes for table `kantor`
--
ALTER TABLE `kantor`
  ADD PRIMARY KEY (`id_kantor`);

--
-- Indexes for table `klausul`
--
ALTER TABLE `klausul`
  ADD PRIMARY KEY (`id_klausul`);

--
-- Indexes for table `login_session`
--
ALTER TABLE `login_session`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  ADD PRIMARY KEY (`id_pertanyaan`);

--
-- Indexes for table `urusan`
--
ALTER TABLE `urusan`
  ADD PRIMARY KEY (`id_urusan`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acara`
--
ALTER TABLE `acara`
  MODIFY `id_acara` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `bagian`
--
ALTER TABLE `bagian`
  MODIFY `id_bagian` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `direktorat`
--
ALTER TABLE `direktorat`
  MODIFY `id_direktorat` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `hasil`
--
ALTER TABLE `hasil`
  MODIFY `id_hasil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `iso`
--
ALTER TABLE `iso`
  MODIFY `id_iso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `kantor`
--
ALTER TABLE `kantor`
  MODIFY `id_kantor` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `klausul`
--
ALTER TABLE `klausul`
  MODIFY `id_klausul` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `login_session`
--
ALTER TABLE `login_session`
  MODIFY `uid` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `pertanyaan`
--
ALTER TABLE `pertanyaan`
  MODIFY `id_pertanyaan` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `urusan`
--
ALTER TABLE `urusan`
  MODIFY `id_urusan` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

DELIMITER $$
--
-- Events
--
CREATE DEFINER=`root`@`localhost` EVENT `cek_status_acara(selesai)` ON SCHEDULE EVERY 1 SECOND STARTS '2018-12-09 21:57:01' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'Ubah status selesai apabila tanggal selesai >= tanggal sekarang' DO UPDATE
    `acara`
SET
    `acara`.`status` = "selesai"
WHERE
    `acara`.`tanggal_selesai` < CURRENT_DATE()
    AND
    `acara`.`status` = "sedang_berjalan"$$

CREATE DEFINER=`root`@`localhost` EVENT `cek_status_acara(berjalan)` ON SCHEDULE EVERY 1 SECOND STARTS '2018-12-09 21:52:21' ON COMPLETION NOT PRESERVE ENABLE COMMENT 'Ubah status acara taggal selesai hari ini' DO UPDATE
    `acara`
SET
    `acara`.`status` = "sedang_berjalan"
WHERE
    `acara`.`tanggal_mulai` <= CURRENT_DATE()
    AND
    `acara`.`status` = "belum_mulai"$$

DELIMITER ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
