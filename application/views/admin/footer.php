<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url ('assets/dist/img/user2-160x160.jpg'); ?> " class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>Admin Bardian</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                    <button type="submit" name="search" id="search-btn" class="btn btn-flat">
                        <i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <li><a href="<?php echo site_url('admin/dashboard')?>"><i class="fa fa-home"></i> <span>Halaman Utama</span></a></li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-edit"></i> <span>Acara</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('admin/lihat_acara')?>"><i class="fa fa-circle-o"></i> Lihat acara</a></li>
                    <li><a href="<?php echo site_url('admin/buat_acara')?>"><i class="fa fa-circle-o"></i> Buat acara</a></li>
                    <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Edit acara</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Laporan</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-circle-o"></i> Lihat laporan</a></li>
                    <li><a href=""><i class="fa fa-circle-o"></i> Buat laporan</a></li>
                    <li><a href=""><i class="fa fa-circle-o"></i> Edit laporan</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-table"></i> <span>SOTK</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-circle-o"></i> Lihat SOTK</a></li>
                    <li><a href="<?php echo site_url('admin/buat_sotk')?>"><i class="fa fa-circle-o"></i> Buat SOTK</a></li>
                    <li><a href=""><i class="fa fa-circle-o"></i> Edit SOTK</a></li>
                </ul>
            </li>
            <li>
                <a href="pages/calendar.html">
                    <i class="fa fa-calendar"></i> <span>Kalender</span>
                    <span class="pull-right-container">
                        <small class="label pull-right bg-red">3</small>
                        <small class="label pull-right bg-blue">17</small>
                    </span>
                </a>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-users"></i> <span>Pengguna</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="<?php echo site_url('admin/lihat_user')?>"><i class="fa fa-circle-o"></i> Lihat pengguna</a></li>
                    <li><a href="<?php echo site_url('admin/buat_user')?>"><i class="fa fa-circle-o"></i> Buat pengguna</a></li>
                    <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Edit pengguna</a></li>
                </ul>
            </li>
            <!-- </li> -->
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-briefcase"></i> <span>Pegawai</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href="pages/forms/advanced.html"><i class="fa fa-circle-o"></i> Lihat pegawai</a></li>
                    <li><a href="<?php echo site_url('admin/buat_user')?>"><i class="fa fa-circle-o"></i> Buat pegawai</a></li>
                    <li><a href="pages/forms/editors.html"><i class="fa fa-circle-o"></i> Edit pegawai</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-exclamation-circle"></i> <span>Daftar Resiko</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-circle-o"></i> Lihat resiko</a></li>
                    <li><a href="<?php echo site_url('admin/buat_resiko')?>"><i class="fa fa-circle-o"></i> Buat resiko</a></li>
                    <li><a href=""><i class="fa fa-circle-o"></i> Edit Resiko</a></li>
                </ul>
            </li>
            <li class="treeview">
                <a href="#">
                    <i class="fa fa-check-circle"></i> <span>ISO</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li><a href=""><i class="fa fa-circle-o"></i> Lihat ISO</a></li>
                    <li><a href="<?php echo site_url('admin/buat_iso')?>"><i class="fa fa-circle-o"></i> Buat ISO</a></li>
                    <li><a href=""><i class="fa fa-circle-o"></i> Edit ISO</a></li>
                </ul>
            </li>
            <li class="header">LABELS</li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<div class="content-wrapper">

    <script type="text/javascript">
        var base_url = "<?php echo base_url() ?>";
        var site_url = "<?php echo site_url() ?>";
    </script>

    <script src="<?php echo base_url ('assets/jquery/dist/jquery.min.js'); ?> "> </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="<?php echo base_url ('assets/bootstrap/dist/js/bootstrap.min.js'); ?> "></script>
    <!-- DataTable -->
    <script src="<?php echo base_url ('assets/datatables.net/js/jquery.dataTables.min.js'); ?>"></script>
    <script src="<?php echo base_url ('assets/datatables.net-bs/js/dataTables.bootstrap.min.js'); ?>"></script>
    <!-- Date Range Picker -->
    <script src="<?php echo base_url ('assets/moment/min/moment.min.js'); ?> "></script>
    <script src="<?php echo base_url ('assets/bootstrap-daterangepicker/daterangepicker.js'); ?> "></script>
    <!-- FastClick -->  
    <script src="<?php echo base_url ('assets/select2/dist/js/select2.full.min.js');?>"></script>
    <script src="<?php echo base_url ('assets/fastclick/lib/fastclick.js'); ?> "> </script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url ('assets/dist/js/adminlte.min.js') ?> "> </script>
    <!-- AdminLTE App -->
    <script src="<?php echo base_url ('assets/js/repeater.js') ?> "> </script>
    <!-- Sparkline -->
    <script src="<?php echo base_url ('assets/jquery-sparkline/dist/jquery.sparkline.min.js') ?> "> </script>
    <!-- jvectormap  -->
    <script src="<?php echo base_url ('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js'); ?> "> </script>
    <script src="<?php echo base_url ('assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js'); ?> "> </script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url ('assets/jquery-slimscroll/jquery.slimscroll.min.js'); ?> "> </script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!--     <script src="<?php echo base_url ('assets/dist/js/pages/dashboard2.js'); ?> "> </script>
 -->    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo base_url ('assets/dist/js/demo.js'); ?> "> </script>
    <?php if(isset($url_page) && $url_page=="buat_sotk"){ ?>
    <script type="text/javascript">
        $('input[type="radio"]').on( "click", function() {
            console.log( $( this ).val() );
            if($( this ).val() == "buat_baru"){
                $("#form_sotk").attr("action","<?php echo site_url('/admin/buat_sotk/input_sotk') ?>");
                $( "#textkantor" ).remove( "#inputkantor" );
                $( "#textkantor" ).html('<input type="text" class="form-control" placeholder="Kantor" id="inputkantor">');
            } else if ($( this ).val() == "sudah_ada"){
                $("#form_sotk").attr("action","<?php echo site_url('/admin/buat_sotk/edit_sotk_kantor') ?>");
                var html = `<select class="form-control" id="inputkantor" name="id_kantor">
                    </select>`; 
                $( "#textkantor" ).remove( "#inputkantor" );

                $( "#textkantor" ).html(html);
                    getKantor();
            }
        });
    </script>
</div>
</script> -->
<?php } else if(isset($url_page) && $url_page=="dashboard") { ?>
<!-- ChartJS -->
<script src="<?php echo base_url ('assets/chart.js/Chart.js'); ?> "> </script>
<?php } else if(isset($url_page) && $url_page=="buat_iso") { ?>
    <script type="text/javascript">
        $('input[type="radio"]').on( "click", function() {
            console.log( $( this ).val() );
            if($( this ).val() == "buat_baru"){
                $("#form_iso").attr("action","<?php echo site_url('/admin/buat_iso/input_iso') ?>");
                $( "#textiso" ).remove( "#inputiso" );
                $( "#textiso" ).html('<input type="text" class="form-control" placeholder="ISO" id="inputiso">');
            } else if ($( this ).val() == "sudah_ada"){
                $("#form_iso").attr("action","<?php echo site_url('/admin/buat_iso/edit_iso') ?>");
                var html = `<select class="form-control" id="inputiso" name="id_iso">
                    </select>`; 
                $( "#textiso" ).remove( "#inputiso" );

                $( "#textiso" ).html(html);
                    getIso();
            }
        });
    </script>
<?php } else if(isset($url_page) && $url_page=="lihat_acara") { ?>
    <script type="text/javascript">
        $(function(){
            $('#dataAcara').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
        })
        $('#modal-share').on('show.bs.modal', function (event) {
          var push = $(event.relatedTarget); // Button that triggered the modal
          var name = push.data('name');
          var id = push.data('id');
          $('.modal-body #modal-nama-acara').html(name);
          $('#form-share').attr("action",site_url+"/admin/lihat_acara/share/"+id);
        })
    </script>
<?php } else if(isset($url_page) && $url_page=="buat_pertanyaan") { ?>
    <script type="text/javascript">
        $(function(){

            $('#dataPertanyaan').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
            <?php 
            $num = sizeof($klausul);
            for ($x = 0; $x < $num; $x++) { ?>
            $('#targetaudit<?php echo $x; ?>').select2();
            $("#repeater<?php echo $x; ?>").createRepeater();
            <?php } ?>
        })

    </script>
<?php } else if(isset($url_page) && $url_page=="lihat_pertanyaan") { ?>
    <script type="text/javascript">
        $(function(){

            $('#dataPertanyaan').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
            <?php 
            $num = sizeof($klausul);
            for ($x = 0; $x < $num; $x++) { ?>
            $('#targetaudit<?php echo $x; ?>').select2();
            $("#repeater<?php echo $x; ?>").createRepeater();
            <?php } ?>
        })

    </script>
<?php } else if(isset($url_page) && $url_page=="ubah_pertanyaan") { ?>
    <script type="text/javascript">
        $(function(){

            $('#dataPertanyaan').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
            <?php 
            $num = sizeof($klausul);
            for ($x = 0; $x < $num; $x++) { ?>
            $('#targetaudit<?php echo $x; ?>').select2();
            $("#repeater<?php echo $x; ?>").createRepeater();
            <?php } ?>
        })

    </script>
<?php } else if(isset($url_page) && $url_page=="lihat_user") { ?>
    <script type="text/javascript">
        $(function(){

            $('#dataUser').DataTable({
              'paging'      : true,
              'lengthChange': false,
              'searching'   : false,
              'ordering'    : true,
              'info'        : true,
              'autoWidth'   : false
            })
        })
    </script>
<?php } else if(isset($url_page) && $url_page=="buat_user") { ?>
    <script type="text/javascript">
        var temp;
        $(function(){
            temp = $("<div></div>").append($("#formauditee")).html();

            if($('#inputPeran').val()=='auditee'){
                $(".box-body").append(temp);
            }else {
                $('#formauditee').remove();
            }
        })
        $('#inputPeran').change(function(){
            var val = $(this).val();
            if(val=='auditee'){
                $(".box-body").append(temp);
            }else {
                $('#formauditee').remove();
            }
        })
    </script>
<?php } ?>



<script>
$(document).ready(function(){
    $("#repeater").createRepeater();
    $('#testselect').select2();
    $("#notif_alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#notif_alert").slideUp(500);
    });
        $("#tanggal").daterangepicker();
});
</script>
<!-- Data -->
<script src="<?php echo base_url ('assets/js/data.js') ?> "> </script>
</body>
</html>