<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Ubah Pertanyaan
        <small>pertanyaan audit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Acara : <?php echo $acara->nama_acara ?></h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form action="<?php echo site_url('admin/lihat_acara/edit_pertanyaan') ?>" method="POST" class="form-horizontal">
                <input type="hidden" name="id_acara" value="<?php echo $acara->id_acara; ?>">
                <input type="hidden" name="sizeklausul" value="<?php echo sizeof($klausul); ?>">
                <div class="box-body">
                  <table id="dataPertanyaan" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Standar / Klausul</th>
                        <th>Pertanyaan</th>
                        <th class="col-xs-4">Target Auditee</th>
                      </tr>
                    </thead> 
                    <tbody>
                    <?php 
                    $num = 0;
                    $status = "";
                    foreach ($klausul as $i => $val) { ?>
                      <tr>
                        <td>
                          <h4><?php echo $val->kode_klausul ?></h4>
                          <p><?php echo $val->deskripsi ?></p>
                          <input type="hidden" name="id_klausul<?php echo $num; ?>" value="<?php echo $val->id_klausul ?>">
                        </td>
                        <td>
                          <h4><strong>Pertanyaan Sebelumnya</strong></h4>
                          <?php foreach ($pertanyaan[$i] as $j => $v) { ?>
                            <p><strong><?php echo ($j+1)." )"; ?></strong> <?php echo $v; ?></p>
                          <?php } ?>

                          <h4><strong>Pertanyaan Baru</strong></h4>
                          <input type="hidden" name="id_pertanyaan[]" value="<?php echo $pertanyaan['id_pertanyaan'][$i]; ?>">
                          <div id="repeater<?php echo $num; ?>">
                            <div class="repeater-heading" align="center">
                                <button type="button" class="btn btn-primary repeater-add-btn"><i class="fa fa-plus"></i> Tambah Pertanyaan</button>
                            </div>
                                
                            
                            <div class="items">
                              <div class="col-xs-12">
                                <textarea name="pertanyaan<?php echo $num; ?>" class="form-control" data-skip-name="true" data-name="pertanyaan<?php echo $num; ?>[]" style="width: 80%;"></textarea>
                                <button id="remove-btn" onclick="$(this).parents('.items').remove()" class="btn btn-danger"><i class="fa fa-close"></i></button>
                              </div>
                            </div>
                          </div>
                        </td>
                        <td>
                            <label>Pilih lebih dari satu( Tidak boleh kosong )</label>
                            <select class="form-control select2" multiple="multiple" id="targetaudit<?php echo $num; ?>" name="targetaudit<?php echo $num++; ?>[]" data-placeholder="Target audit" required=""
                                    style="width: 100%;">
                                    <?php foreach ($targetaudit as $j => $v) {
                                      if($v->tabel == "kantor") {
                                        $status = "Kantor";
                                      } else if($v->tabel == "direktorat") {
                                        $status = "Direktorat";
                                      } else if($v->tabel == "bagian") {
                                        $status = "Bagian";
                                      }
                                      $value = $v->tabel."_".$v->id;
                                      $found = 0;
                                      foreach ($targetauditchosen[$i] as $k => $tac) {
                                        if($value==$tac) {
                                          echo '<option selected value="'.$value.'">'.$v->identifier.' ( '.$status.' )</option>';
                                          $found = 1;
                                          break;
                                        }
                                      }
                                      if($found!=1){
                                        echo '<option value="'.$value.'">'.$v->identifier.' ( '.$status.' )</option>';
                                      }
                                    }
                                    ?>
                            </select>
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php echo site_url('admin/lihat_acara'); ?>" class="btn btn-default">Cancel</a>
                  <button type="submit" id="btnSubmit" class="btn btn-info pull-right">Submit</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
        </div>
      </div>
    </section>
</div>
