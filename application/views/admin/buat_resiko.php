<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Horizontal Form</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form class="form-horizontal">
                    <div class="box-body">
                        <h4 class="box-title">No. Reg</h4>
                        <div class="form-group">
                            <label for="inputBagian" class="col-sm-2 control-label">Bagian</label>
                            <div class="col-sm-10">
                                <select class="form-control">
                                    <option>Option 1</option>
                                    <option>Option 2</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputProsedur" class="col-sm-2 control-label">Prosedur</label>
                            <div class="col-sm-10">
                                <select class="form-control">
                                    <option>Option 1</option>
                                    <option>Option 2</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputResiko" class="col-sm-2 control-label">Resiko</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" placeholder="Masukkan resiko"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputHarapan" class="col-sm-2 control-label">Harapan</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" placeholder="Masukkan harapan"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputDampak" class="col-sm-2 control-label">Dampak</label>

                            <div class="col-sm-10">
                                <textarea class="form-control" rows="3" placeholder="Masukkan dampak"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputProsedur" class="col-sm-2 control-label">Probabilitas</label>
                            <div class="col-sm-10">
                                <select class="form-control">
                                    <option>Option 1</option>
                                    <option>Option 2</option>
                                    <option>Option 3</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputProsedur" class="col-sm-2 control-label">Keparahan</label>
                            <div class="col-sm-10">
                                <select class="form-control">
                                    <option>Option 1</option>
                                    <option>Option 2</option>
                                    <option>Option 3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info center-block">Submit</button>
                    </div>
                    <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>
