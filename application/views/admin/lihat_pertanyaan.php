<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Lihat Pertanyaan
        <small>pertanyaan audit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Acara : <?php echo $acara->nama_acara ?></h3>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <table id="dataPertanyaan" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Standar / Klausul</th>
                        <th class="col-xs-6">Pertanyaan</th>
                        <th class="col-xs-4">Target Auditee</th>
                      </tr>
                    </thead> 
                    <tbody>
                    <?php 
                    $num = 0;
                    $status = "";
                    foreach ($klausul as $i => $val) { ?>
                      <tr>
                          
                        
                        <td>
                          <h4><?php echo $val->kode_klausul ?></h4>
                          <p><?php echo $val->deskripsi ?></p>
                          <input type="hidden" name="id_klausul<?php echo $num; ?>" value="<?php echo $val->id_klausul ?>">
                        </td>
                        <td>
                          <?php foreach ($pertanyaan[$i] as $j => $v) { ?>
                            <p><strong><?php echo ($j+1)." )"; ?></strong> <?php echo $v; ?></p>
                          <?php } ?>
                        </td>
                        <td>
                            <select disabled class="form-control select2" multiple="multiple" id="targetaudit<?php echo $num; ?>" name="targetaudit<?php echo $num++; ?>[]" data-placeholder="Target audit" required=""
                                    style="width: 100%;">
                                    <?php foreach ($targetaudit[$i] as $j => $v) {
                                      if($v->tabel == "kantor") {
                                        $status = "Kantor";
                                      } else if($v->tabel == "direktorat") {
                                        $status = "Direktorat";
                                      } else if($v->tabel == "bagian") {
                                        $status = "Bagian";
                                      }
                                      $value = $v->tabel."_".$v->id;
                                      echo '<option selected value="'.$value.'">'.$v->identifier.' ( '.$status.' )</option>';
                                    } ?>
                            </select>
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php echo site_url('admin/lihat_acara'); ?>" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>
