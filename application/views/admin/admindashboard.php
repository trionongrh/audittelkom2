<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">

    <?php 
    echo $this->session->flashdata('msg');
    ?>

        <h1>
            General Form Elements
            <small>Preview</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Forms</a></li>
            <li class="active">General Elements</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    	<form action="<?php echo site_url('/admin/dashboard/tes'); ?>" method="post">
    	<div class="col-md-6">
              <div class="form-group">
                <label>Multiple</label>
                <select class="form-control select2" multiple="multiple" id="testselect" name="test[]" data-placeholder="Select a State"
                        style="width: 100%;">
					<option>Alabama</option>
					<option>Alaska</option>
					<option>California</option>
					<option>Delaware</option>
					<option>Tennessee</option>
					<option>Texas</option>
					<option>Washington</option>
                </select>
              </div>
              <button type="submit">Ok</button>	
		</form>    
    </section>
</div>
