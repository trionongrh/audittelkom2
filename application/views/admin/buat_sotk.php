<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <?php 
    echo $this->session->flashdata('msg');
    ?>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="col-md-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Horizontal Form</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <form action="<?php echo site_url('/admin/buat_sotk/input_iso')?>" class="form-horizontal" method="post" id="form_sotk">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="inputUsername" class="col-sm-2 control-label"></label>

                            <div class="col-sm-10">
                                <label> Pilih nama kantor</label>
                                <div class="radio">
                                    <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="buat_baru" checked="">
                                    Buat baru
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="sudah_ada">
                                    Pilih yang sudah ada
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="inputUsername" class="col-sm-2 control-label">Nama Kantor</label>

                            <div class="col-sm-10" id="textkantor">
                                <input type="text" class="form-control" name="nama_kantor" placeholder="Kantor" id="inputkantor" >
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="inputUsername" class="col-sm-2 control-label">Nama direktorat</label>

                            <div class="col-sm-10">
                                <div id="repeater">
                                    <div class="repeater-heading" align="right">
                                        <button type="button" class="btn btn-primary repeater-add-btn">Tambah direktorat</button>
                                    </div>
                                    <div class="clearfix"></div>
                                        
                                    <div class="items">
                                        <div class="item-content">
                                            <div class="row">
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" id="inputdirektorat[]" placeholder="direktorat" name="nama_direktorat"data-skip-name="true" data-name="direktorat[]" required>
                                                        <div class="input-group-btn">
                                                            <button id="remove-btn" onclick="$(this).parents('.items').remove()" class="btn btn-danger">X</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                    <div class="box-footer">
                        <button type="submit" class="btn btn-info center-block">Next</button>
                    </div>
                <!-- /.box-footer -->
                </form>
            </div>
        </div>
    </section>
</div>
