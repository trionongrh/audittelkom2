<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        User
        <small>list data user</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <!-- <h3 class="box-title">Horizontal Form</h3> -->
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataUser" class="table table-bordered table-hover">
                    <thead>
                    <tr>
                      <th>No</th>
                      <th>Username</th>
                      <th>Level</th>
                      <th>SOTK</th>
                    </tr>
                    </thead>
                    <tbody>
                      <?php 
                      $num = 1;
                      foreach ($user as $i => $val) { ?>
                        <tr>
                          <td><?php echo $num++;?></td>
                          <td><?php echo $val['username'];?></td>
                          <td><?php echo $val['level'];?></td>
                          <td><?php if(isset($sotk[$i]['nama_sotk'])) echo $sotk[$i]['nama_sotk'].' ( '.ucfirst($sotk[$i]['tabel']).' )';?></td>
                        </tr>
                      <?php } ?>
                    </tbody>
                  </table>
                </div>
                <div class="box-footer">
                  <a href="<?php echo site_url('admin/buat_user'); ?>" class="btn btn-default pull-right">Buat User</a>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>