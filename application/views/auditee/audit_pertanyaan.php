<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Audit
        <small>pertanyaan audit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Acara : <?php echo $acara->nama_acara ?></h3>
              </div>
              <!-- /.box-header -->
                <div class="box-body">
                  <table id="dataPertanyaan" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Standar / Klausul</th>
                        <th class="col-xs-6">Pertanyaan</th>
                        <th class="col-xs-4">Status</th>
                      </tr>
                    </thead> 
                    <tbody>
                    <?php 
                    $num = 0;
                    $status = "";
                    foreach ($klausul as $i => $val) { ?>
                      <tr>
                          
                        
                        <td>
                          <h4><?php echo $val->kode_klausul ?></h4>
                          <p><?php echo $val->deskripsi ?></p>
                          <input type="hidden" name="id_klausul<?php echo $num; ?>" value="<?php echo $val->id_klausul ?>">
                        </td>
                        <td>
                          <table width="100%">
                            <tbody>
                          <?php foreach ($pertanyaan[$i] as $j => $v) { ?>
                            <tr>
                              <td class="kolomPertanyaan">
                              <p><strong><?php echo ($j+1)." )"; ?></strong> <?php echo $v; ?></p>
                              <textarea placeholder="Jawaban..." class="form-control" name="jawaban<?php echo $num; ?>[]" style="width: 100%;"></textarea>
                              </td>
                            </tr>
                          <?php } ?>
                            </tbody>
                          </table>
                        </td>
                        <td>
                          <table width="100%">
                            <tbody>
                          <?php foreach ($pertanyaan[$i] as $j => $v) { ?>
                            <tr>
                              <td class="kolomStatus">
                                <select name="status<?php echo $i; ?>[]" class="form-control">
                                  <option value="0">0 - belum memenuhi</option>
                                  <option value="1">1 - sudah memenuhi</option>
                                </select>
                              </td>
                            </tr>
                          <?php } ?>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php echo site_url('auditee/lihat_acara'); ?>" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>
