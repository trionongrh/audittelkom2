<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Buat Pertanyaan
        <small>pertanyaan audit</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <div class="box box-info">
              <div class="box-header with-border">
                <h3 class="box-title">Acara : <?php echo $acara->nama_acara ?></h3>
              </div>
              <!-- /.box-header -->
              <!-- form start -->
              <form action="<?php echo site_url('auditor/lihat_acara/test') ?>" method="POST" class="form-horizontal">
                <input type="hidden" name="sizeklausul" value="<?php echo sizeof($klausul); ?>">
                <div class="box-body">
                  <table id="dataPertanyaan" class="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>Standar / Klausul</th>
                        <th>Pertanyaan</th>
                        <th class="col-xs-4">Target Auditee</th>
                      </tr>
                    </thead> 
                    <tbody>
                    <?php 
                    $num = 0;
                    foreach ($klausul as $i => $val) { ?>
                      <tr>
                          
                        
                        <td>
                          <h4><?php echo $val->kode_klausul ?></h4>
                          <p><?php echo $val->deskripsi ?></p>
                        </td>
                        <td>
                          <textarea name="pertanyaan<?php echo $num; ?>" class="form-control" style="width: 100%;"></textarea>
                        </td>
                        <td>
                            <label>Pilih lebih dari satu</label>
                            <select class="form-control select2" multiple="multiple" id="targetaudit<?php echo $num; ?>" name="targetaudit<?php echo $num++; ?>[]" data-placeholder="Target audit"
                                    style="width: 100%;">
                                    <?php foreach ($targetaudit as $i => $v) {
                                      $value = $v->tabel."_".$v->id;
                                      echo '<option value="'.$value.'">'.$v->identifier.'</option>';
                                    } ?>
                            </select>
                        </td>
                      </tr>
                    <?php } ?>
                    </tbody>
                  </table>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                  <a href="<?php echo site_url('auditor/lihat_acara'); ?>" class="btn btn-default">Cancel</a>
                  <button type="submit" class="btn btn-info pull-right">Submit</button>
                </div>
                <!-- /.box-footer -->
              </form>
            </div>
        </div>
      </div>
    </section>
</div>
