<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Acara
        <small>list data acara</small>
      </h1>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
            <!-- Ho rizontal Form -->
            <div class="box box-info">
                <div class="box-header">
                    <!-- <h3 class="box-title">Horizontal Form</h3> -->
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="box-body">
                  <table id="dataAcara" class="table table-bordered table-hover">
                <thead>
                <tr>
                  <th>No</th>
                  <th>ISO</th>
                  <th>Nama Acara</th>
                  <th>Tanggal Acara</th>
                  <th>Pertanyaan</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php 
                  $num = 1;
                  foreach ($acara as $i => $val) { 
                    if($val->status != "selesai") {
                    ?>

                    <tr>
                      <td><?php echo $num++; ?></td>
                      <td><?php echo $val->nama_iso ?></td>
                      <td><?php echo $val->nama_acara ?></td>
                      <td>
                      <?php
                        $status = "";
                        if($val->status == "belum_mulai"){
                          $status = '<small class="label bg-green">Belum Mulai</small>';
                        } else if ($val->status == "sedang_berjalan") {
                          $status = '<small class="label bg-blue">Sedang Berlangsung</small>';
                        } else if ($val->status == "selesai") {
                          $status = '<small class="label bg-green">Selesai</small>';
                        }
                        $date_start = date_create($val->tanggal_mulai);
                        $date_end = date_create($val->tanggal_selesai);
                        echo date_format($date_start, "d/m/Y").' - '.date_format($date_end, "d/m/Y").' '.$status;
                      ?>
                      </td>
                      <td>
                        <?php if($val->pertanyaan == "0"){
                          echo '<small class="label bg-yellow">Belum</span>';
                        } else {
                          echo '<small class="label bg-green">Sudah</span>';
                        }?>
                      </td>
                      <?php if($_SESSION['level'] == 'admin' || $_SESSION['level'] == 'auditor') { ?>
                        <td>
                          <div class="btn-group">
                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                              <i class="fa fa-cog"></i>
                            </button>
                            <ul class="dropdown-menu" role="menu">
                              <?php if($val->pertanyaan == "0"){ ?>
                                <li><a href="<?php echo site_url('auditor/lihat_acara/buat_pertanyaan/'.$val->id_acara) ?>"><i class="fa fa-file-o"></i>Buat Pertanyaan</a></li>
                              <?php } else { ?>
                                <?php if($val->status == "sedang_berjalan") { ?>
                                <li><a href="#"><i class="fa fa-share"></i>Sebarkan</a></li>
                                
                                <?php } ?>
                              <?php } ?>
                            </ul>
                          </div>
                        </td>
                      <?php } ?>
                    </tr>    
                  <?php } } ?>
                </tbody>
              </table>
                </div>
            </div>
        </div>
      </div>
    </section>
</div>
