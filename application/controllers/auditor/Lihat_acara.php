<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_acara extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_acara');

	}
	public function index()
	{
		$data['acara'] = $this->model_acara->listing();
		$footer['url_page'] = 'lihat_acara';
		$this->load->view('auditor/header');
		$this->load->view('auditor/lihat_acara', $data);
		$this->load->view('auditor/footer', $footer);
	}

	public function buat_pertanyaan($id_acara){
		$data['acara'] = $this->model_acara->detailacara($id_acara);
		if($data['acara']->pertanyaan == '1') {
			redirect('auditor/lihat_acara');
		}
		$data['klausul'] = $this->model_acara->detailklausul($id_acara);
		$data['targetaudit'] = $this->model_acara->target_audit();

		
		$footer['url_page'] = 'buat_pertanyaan';
		$this->load->view('auditor/header');
		$this->load->view('auditor/buat_pertanyaan', $data);
		$this->load->view('auditor/footer', $footer);

	}
}
