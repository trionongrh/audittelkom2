<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_acara extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="auditee") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_acara');
		$this->load->model('model_pertanyaan');
	}
	public function index()
	{
		$data['acara'] = $this->model_acara->listing_auditee();
 		$footer['url_page'] = 'lihat_acara';
		$this->load->view('auditee/header');
		$this->load->view('auditee/lihat_acara', $data);
		$this->load->view('auditee/footer', $footer);
	}
	public function audit_pertanyaan($id_acara){
		$data['acara'] = $this->model_acara->detailacara($id_acara);
		if(isset($data['acara'])){
			if($data['acara']->publish == '0') {
				redirect('auditee/lihat_acara');
				exit();
			}
		}else{
			redirect('auditee/lihat_acara');
			exit();
		}
		$pertanyaan = $this->model_pertanyaan->detail_pertanyaan($id_acara);
		$data['targetauditchosen'] = array();
		foreach ($pertanyaan as $i => $val) {
			$data['pertanyaan'][$i] = json_decode($val->pertanyaan);
			$data['targetauditchosen'][$i] = json_decode($val->target_auditee);
		}
		$data['targetaudit'] = array();
		$temp = array (
			'id' => '',
			'tabel' => ''
		);
		foreach ($data['targetauditchosen'] as $i => $val) {
			foreach ($val as $j => $val2) {
				$temp['id'] = explode("_", $val2)[1];
				$temp['tabel'] = explode("_", $val2)[0];
				$data['targetaudit'][$i][$j] = 	$this->model_pertanyaan->target_audit_choose($temp['id'],$temp['tabel']);	
			}
		}
		$data['klausul'] = $this->model_acara->detailklausul($id_acara);
		$footer['url_page'] = 'audit_pertanyaan';
		$this->load->view('auditee/header');
		$this->load->view('auditee/audit_pertanyaan', $data);
		$this->load->view('auditee/footer', $footer);
	}

	public function test(){
		echo '<pre>';
		var_dump($_SESSION);
		echo '</pre>';
		exit();
	}
}