<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {


	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')!="") {
			if($this->session->userdata('level')=='admin'){
				redirect('admin/dashboard');
			}
			else if($this->session->userdata('level')=='auditor'){
				redirect('auditor/dashboard');
			}
			else if($this->session->userdata('level')=='auditee'){
				redirect('auditee/dashboard');
			}
		}
		$this->load->helper('text');
	}
	public function index()
	{
		$this->load->view('login');
	}

public function cek_login() {
		$data = array('username' => $this->input->post('username', TRUE),
						'password' => ($this->input->post('pass', TRUE))
			);

		$this->load->model('model_user'); // load model_user
		$hasil = $this->model_user->cek_user($data);
		if ($hasil->num_rows() == 1) {
			foreach ($hasil->result() as $sess) {
				$sess_data['logged_in'] = 'Sudah Loggin';
				$sess_data['uid'] = $sess->uid;
				$sess_data['username'] = $sess->username;
				$sess_data['level'] = $sess->level;
				$this->session->set_userdata($sess_data);
			}
			if($this->session->userdata('level')=='admin'){
				redirect('admin/dashboard');
			}
			else if($this->session->userdata('level')=='auditor'){
				redirect('auditor/dashboard');
			}
			else if($this->session->userdata('level')=='auditee'){
				redirect('auditee/dashboard');
			}
		}
		else {
			echo "<script>alert('Gagal login: Cek username, password!');history.go(-1);</script>";
		}
	}

}
?>
