<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	
	// Load database
	public function __construct(){
	parent::__construct();
	$this->load->model('Tampil_Auditee');

	public function index() {
		$site	= $this->Tampil_Auditee->kantor();
		
		$data	= array( 'nama_kantor'	=> $site['id_kantor'].' | '.$site['tagline'],
						 'nama_direktur' => $site['id_direktur'].', '.$site['keywords'],
						 'nama_bagian'	=> $site['id_bagian']
						 'nama_urusan'	=> $site['id_urusan']
						 
		$this->load->view('layout/wrapper',$data); 
	}
}