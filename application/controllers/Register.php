<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
 
    class Register extends CI_Controller {
     
        public function __construct() {
            parent::__construct();
            if ($this->session->userdata('username')!="") {
                redirect('dashboard');
            }
            $this->load->helper('text');
        }
        public function index()
        {
            $this->load->view('admin/buat_user');
        }

        public function daftar()
            $data = array('username' => $this->form_validation->set_rules('username', 'USERNAME','required');
                                        $this->form_validation->set_rules('password','PASSWORD','required');
                                        if($this->form_validation->run() == FALSE) {
                                            $this->load->view('admin/buat_user');
                                        }else{

                                        $data['username'] =    $this->input->post('username');
                                        $data['password'] =    ($this->input->post('password'));

                                        $this->m_account->daftar($data);

                                        $pesan['message'] =    "Pendaftaran berhasil";

                                        $this->load->view('admin/buat_user',$pesan);
            }
        }
    }
?>