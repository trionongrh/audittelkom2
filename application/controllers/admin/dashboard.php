<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
	}
	public function index()
	{
		$footer['url_page'] = "dashboard";
		$this->load->view('admin/header');
		$this->load->view('admin/admindashboard');
		$this->load->view('admin/footer',$footer);
	}

	public function tes()
	{
		$post=$this->input->post();
		echo '<pre>';
		var_dump($post);
		echo '</pre>';
	}
}