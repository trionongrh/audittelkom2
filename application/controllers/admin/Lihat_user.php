<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_user extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_user'); // load model_user
		$this->load->model('model_sotk'); // load model_user
	}
	public function index()
	{
		$data['user'] = $this->model_user->getUser();
		$data['sotk'] = array();
		foreach ($data['user'] as $i => $v) {
			if($v['sotk']!=""){
				$data['sotk'][$i] = $this->model_sotk->getSOTK($v['sotk']);
			}
		}
		
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();
		$footer['url_page'] = 'lihat_user';
		$this->load->view('admin/header');
		$this->load->view('admin/lihat_user',$data);
		$this->load->view('admin/footer',$footer);
	}
}
