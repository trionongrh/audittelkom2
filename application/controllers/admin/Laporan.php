<?php
class Laporan extends CI_Controller{
    function __construct(){
      parent::__construct();
      //load chart_model from model
      $this->load->model('Model_laporan');
    }

    function index(){
      $data = $this->Model_laporan->get_data()->result();
      $x['data'] = json_encode($data);
      $this->load->view('admin/laporan',$x);
    }

    public function laporan_pdf(){

     $data = $this->Model_laporan->get_data()->result();
      $x['data'] = json_encode($data);
      $this->load->view('admin/laporan',$x);
    // $data = array(
    //   "dataku" => array(
    //     "nama" => "Petani Kode",
    //     "url" => "http://petanikode.com"
    //   )
   // );

    $this->load->library('pdf');
    $this->pdf->setPaper('A4', 'potrait');
    $this->pdf->filename = "laporan-audit.pdf";
    $this->pdf->load_view('admin/laporan',$x);


  }
}
