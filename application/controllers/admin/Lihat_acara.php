<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lihat_acara extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
		$this->load->model('model_acara');
		$this->load->model('model_pertanyaan');
	}
	public function index()
	{
		$data['acara'] = $this->model_acara->listing();
		$footer['url_page'] = 'lihat_acara';
		$this->load->view('admin/header');
		$this->load->view('admin/lihat_acara', $data);
		$this->load->view('admin/footer', $footer);
	}

	public function buat_pertanyaan($id_acara){

		$data['acara'] = $this->model_acara->detailacara($id_acara);
		if(isset($data['acara'])){
			if($data['acara']->pertanyaan == '1') {
				redirect('admin/lihat_acara');
				exit();
			}
		}else{
			redirect('admin/lihat_acara');
			exit();
		}
		$data['klausul'] = $this->model_acara->detailklausul($id_acara);

		$data['targetaudit'] = $this->model_pertanyaan->target_audit();
		// echo '<pre>';
		// var_dump($data['targetaudit']);
		// echo '</pre>';
		// exit();

		$footer['url_page'] = 'buat_pertanyaan';
		$this->load->view('admin/header');
		$this->load->view('admin/buat_pertanyaan', $data);
		$this->load->view('admin/footer', $footer);

	}

	public function input_pertanyaan(){
		$inputpost = $this->input->post();
		$size = $inputpost['sizeklausul'];


		// echo '<pre>';
		// var_dump($inputpost);
		// echo '</pre>';
		// exit();

		$this->model_acara->editacara($inputpost['id_acara'],array('pertanyaan' =>  '1'));
		for($x = 0; $x<$size; $x++){
			if(isset($inputpost['targetaudit'.$x])) {
				$data = array(
					'id_klausul' => $inputpost['id_klausul'.$x], 
					'id_acara' => $inputpost['id_acara'], 
					'pertanyaan' => json_encode($inputpost['pertanyaan'.$x]), 
					'target_auditee' => json_encode($inputpost['targetaudit'.$x]), 
				);
				$this->model_pertanyaan->tambah_pertanyaan($data);
			} else {
				redirect('admin/lihat_acara/buat_pertanyaan/'.$inputpost['id_acara']);
				break;
			}
		}
		redirect('admin/lihat_acara');
	}

	public function lihat_pertanyaan($id_acara){
		$data['acara'] = $this->model_acara->detailacara($id_acara);
		if(isset($data['acara'])){
			if($data['acara']->pertanyaan == '0') {
				redirect('admin/lihat_acara');
				exit();
			}
		}else{
			redirect('admin/lihat_acara');
			exit();
		}
		$pertanyaan = $this->model_pertanyaan->detail_pertanyaan($id_acara);
		$data['targetauditchosen'] = array();
		foreach ($pertanyaan as $i => $val) {
			$data['pertanyaan'][$i] = json_decode($val->pertanyaan);
			$data['targetauditchosen'][$i] = json_decode($val->target_auditee);
		}
		$data['targetaudit'] = array();
		$temp = array (
			'id' => '',
			'tabel' => ''
		);
		foreach ($data['targetauditchosen'] as $i => $val) {
			foreach ($val as $j => $val2) {
				$temp['id'] = explode("_", $val2)[1];
				$temp['tabel'] = explode("_", $val2)[0];
				$data['targetaudit'][$i][$j] = 	$this->model_pertanyaan->target_audit_choose($temp['id'],$temp['tabel']);	
			}
		}
		$data['klausul'] = $this->model_acara->detailklausul($id_acara);
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();

		$footer['url_page'] = 'lihat_pertanyaan';
		$this->load->view('admin/header');
		$this->load->view('admin/lihat_pertanyaan', $data);
		$this->load->view('admin/footer', $footer);
	}

	public function share($id_acara){
		$this->model_acara->editacara($id_acara,array('publish' =>  '1'));
		redirect('admin/lihat_acara');
	}

	public function ubah_pertanyaan($id_acara){
		$pertanyaan = $this->model_pertanyaan->detail_pertanyaan($id_acara);
		$data['acara'] = $this->model_acara->detailacara($id_acara);
		if(isset($data['acara'])){
			if($data['acara']->pertanyaan == '0') {
				redirect('admin/lihat_acara');
				exit();
			}
		}else{
			redirect('admin/lihat_acara');
			exit();
		}
		$data['targetauditchosen'] = array();
		foreach ($pertanyaan as $i => $val) {
			$data['pertanyaan']['id_pertanyaan'][$i] = $val->id_pertanyaan;
			$data['pertanyaan'][$i] = json_decode($val->pertanyaan);
			$data['targetauditchosen'][$i] = json_decode($val->target_auditee);
		}
		$data['klausul'] = $this->model_acara->detailklausul($id_acara);

		$data['targetaudit'] = $this->model_pertanyaan->target_audit();
		// echo '<pre>';
		// var_dump($data);
		// echo '</pre>';
		// exit();
		$footer['url_page'] = 'ubah_pertanyaan';
		$this->load->view('admin/header');
		$this->load->view('admin/ubah_pertanyaan', $data);
		$this->load->view('admin/footer', $footer);
	}
	public function edit_pertanyaan(){
		$inputpost = $this->input->post();
		$size = $inputpost['sizeklausul'];


		// echo '<pre>';
		// var_dump($inputpost);	
		// echo '</pre>';
		// exit();

		for($x = 0; $x<$size; $x++){
			if(isset($inputpost['targetaudit'.$x])) {
				$data = array(
					'target_auditee' => json_encode($inputpost['targetaudit'.$x])
				);
				$pertanyaan  = array();;
				foreach ($inputpost['pertanyaan'.$x] as $i => $val) {
					if($val!=''){
						$pertanyaan[$i] = $val;
					}
				}
				if(!empty($pertanyaan)){
					$data['pertanyaan'] = json_encode($pertanyaan);
				}
				$this->model_pertanyaan->editpertanyaan($inputpost['id_pertanyaan'][$x],$data);
			} else {
				redirect('admin/lihat_acara/edit_pertanyaan/'.$inputpost['id_acara']);
				break;
			}
		}
		redirect('admin/lihat_acara/lihat_pertanyaan/'.$inputpost['id_acara']);
	}
}
