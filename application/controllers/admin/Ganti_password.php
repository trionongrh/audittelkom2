<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ganti_password extends CI_Controller {

public function save_password()
 { 
  $this->form_validation->set_rules('new','New','required|alpha_numeric');
  $this->form_validation->set_rules('re_new', 'Retype New', 'required|matches[new]');
    if($this->form_validation->run() == FALSE)
  {
   $this->load->view('layout/header');
   $this->load->view('account/change-pass');
   $this->load->view('layout/footer');
  }else{
   $cek_old = $this->model_account->cek_old();
   if ($cek_old == False){
    $this->session->set_flashdata('error','Old password not match!' );
    $this->load->view('layout/header');
    $this->load->view('account/change-pass');
    $this->load->view('layout/footer');
   }else{
    $this->model_account->save();
    $this->session->sess_destroy();
    $this->session->set_flashdata('error','Your password success to change, please relogin !' );
    $this->load->view('layout/header');
    $this->load->view('layout/login_sign_up');
    $this->load->view('layout/footer');
   }//end if valid_user
  }
 }