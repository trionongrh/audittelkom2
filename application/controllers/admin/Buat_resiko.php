<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Buat_resiko extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="" || $this->session->userdata('level')!="admin") {
			redirect('Login');
		}
		$this->load->helper('text');
	}
	public function index()
	{
		$this->load->view('admin/header');
		$this->load->view('admin/buat_resiko');
		$this->load->view('admin/footer');
	}
}
