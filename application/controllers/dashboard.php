<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	public function __construct() {
		parent::__construct();
		if ($this->session->userdata('username')=="") {
			redirect('Login');
		}
		$this->load->helper('text');
	}
	public function index()
	{
		if($this->session->userdata('level')=='admin'){
		$this->load->view('admin/header');
		$this->load->view('admin/footer');	
	}
	else
	{
		if($this->session->userdata('level')=='auditor'){
		$this->load->view('auditor/header');
		$this->load->view('auditor/footer');
	}
	else
	{
		if($this->session->userdata('level')=='auditee'){
		$this->load->view('auditee/header');
		$this->load->view('auditee/footer');
	}
	}
}
}
}