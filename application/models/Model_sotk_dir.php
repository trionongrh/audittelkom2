<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_sotk_dir extends CI_Model {

	
	// Load database
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	//Listing
	public function listing() {
		$this->db->select('*');
		$this->db->from('direktorat');
		$this->db->order_by('id_direktorat','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function listingwhere($id_kantor) {
		$this->db->select('*');
		$this->db->from('direktorat');
		$this->db->order_by('id_direktorat','ASC');
		$this->db->where('id_kantor',$id_kantor);
		$query = $this->db->get();
		return $query->result();
	}


	// read perkategori_produk
	public function read($nama_direktorat){
		$query = $this->db->get_where('direktorat',array('nama_direktorat'  => $nama_direktorat));
		return $query->row();
	}
	
	// detail perkategori_produk
	public function detail($id_direktorat){
		$query = $this->db->get_where('direktorat',array('id_direktorat'  => $id_direktorat));
		return $query->row();
	}
	
	// Tambah
	public function tambah ($data) {
		$this->db->insert('direktorat',$data);
	}
	
	// Edit 
	public function edit ($data) {
		$this->db->where('id_direktorat',$data['id_direktorat']);
		$this->db->update('direktorat',$data);
	}
	
	// Delete
	public function delete ($data){
		$this->db->where('id_direktorat',$data['id_direktorat']);
		$this->db->delete('direktorat',$data);
	}
}