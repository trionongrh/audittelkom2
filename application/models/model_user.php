<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	class Model_user extends CI_Model {

		public function cek_user($data) {
			$query = $this->db->get_where('login_session', $data);
			return $query;
		}

		public function getUser(){
			$this->db->select('uid,username,level,sotk');
			$this->db->from('login_session');
			$query = $this->db->get();
			return $query->result_array();
		}

		public function insertUser($data){
			return $this->db->insert('login_session',$data);
		}
		public function check_username($username){
			$query = $this->db->get_where('login_session', array('username' => $username));
			return $query->row_array();
		}

	}

?>