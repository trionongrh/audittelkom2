<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_acara extends CI_Model {

	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function listing() {
		$this->db->select('acara.*, iso.nama_iso');
		$this->db->from('acara');
		$this->db->join('iso', 'iso.id_iso=acara.id_iso');
		$this->db->order_by('id_acara','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	public function listing_auditee() {
		$this->db->select('acara.*, iso.nama_iso');
		$this->db->from('acara');
		$this->db->join('iso', 'iso.id_iso=acara.id_iso');
		$this->db->where('publish','1');
		$this->db->where('status','sedang_berjalan');
		$this->db->order_by('id_acara','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	
	// read perkategori_produk
	public function read($nama_acara){
		$query = $this->db->get_where('acara',array('nama_acara'  => $nama_acara));
		return $query->row();
	}

	public function detailacara($id_acara){
		$query = $this->db->get_where('acara',array('id_acara'  => $id_acara));
		return $query->row();
	}
	
	// detail perkategori_produk
	public function detailklausul($id_acara){
		$this->db->select('acara.*, iso.nama_iso, klausul.*');
		$this->db->from('acara');
		$this->db->join('iso', 'iso.id_iso=acara.id_iso');
		$this->db->join('klausul', 'klausul.id_iso=iso.id_iso');
		$this->db->where('acara.id_acara', $id_acara);
		$this->db->order_by('id_acara','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	
	// Tambah
	public function tambahacara($data) {
		$this->db->insert('acara',$data);
	}



	// Tambah
	public function tambahreturnID ($data) {
		$this->db->insert('acara',$data);
		return $this->db->insert_id();
	}
	
	// Edit 
	public function editacara($idacara,$data) {
		$this->db->where('id_acara',$idacara);
		$this->db->update('acara',$data);
	}
	
	// Delete
	public function delete ($data){
		$this->db->where('id_acara',$data['id_acara']);
		$this->db->delete('acara',$data);
	}
}