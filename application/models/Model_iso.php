<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_iso extends CI_Model {

	
	// Load database
	public function __construct() {
		parent::__construct();
		$this->load->database();
	}
	
	//Listing
	public function listing() {
		$this->db->select('*');
		$this->db->from('iso');
		$this->db->order_by('id_iso','ASC');
		$query = $this->db->get();
		return $query->result();
	}
	
	// read perkategori_produk
	public function read($nama_iso){
		$query = $this->db->get_where('iso',array('nama_iso'  => $nama_iso));
		return $query->row();
	}
	
	// detail perkategori_produk
	public function detail($id_iso){
		$query = $this->db->get_where('iso',array('id_iso'  => $id_iso));
		return $query->row();
	}
	
	// Tambah
	public function tambah ($data) {
		$this->db->insert('iso',$data);
	}

	// Tambah
	public function tambahreturnID ($data) {
		$this->db->insert('iso',$data);
		return $this->db->insert_id();
	}
	
	// Edit 
	public function edit ($data) {
		$this->db->where('id_iso',$data['id_iso']);
		$this->db->update('iso',$data);
	}
	
	// Delete
	public function delete ($data){
		$this->db->where('id_iso',$data['id_iso']);
		$this->db->delete('iso',$data);
	}
	public function readklausul($kode_klausul){
		$query = $this->db->get_where('klausul',array('kode_klausul'  => $kode_klausul));
		return $query->row();
	}
	public function tambahklausul($data) {
		$this->db->insert('klausul',$data);
	}
	// Edit 
	public function editklausul($data) {
		$this->db->where('kode_klausul',$data['kode_klausul']);
		$this->db->update('klausul',$data);
	}
	
	// Delete
	public function deleteklausul($data){
		$this->db->where('kode_klausul',$data['kode_klausul']);
		$this->db->delete('klausul',$data);
	}
}