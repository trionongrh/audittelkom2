<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pertanyaan extends CI_Model {


	public function __construct() {
		parent::__construct();
		$this->load->database();
	}

	public function detail_pertanyaan($id_acara) {
		$this->db->select('pertanyaan.*, klausul.*');
		$this->db->from('pertanyaan');
		$this->db->join('klausul', 'klausul.id_klausul=pertanyaan.id_klausul');
		$this->db->where('pertanyaan.id_acara',$id_acara);
		$this->db->order_by('id_pertanyaan','ASC');
		$query = $this->db->get();
		return $query->result();
	}

	public function target_audit(){
		$sql = "
		SELECT * FROM
		( SELECT
			id_kantor as id,
			nama_kantor as identifier,
			'kantor' as tabel
		FROM kantor
		UNION ALL
		SELECT
			id_direktorat as id,
			nama_direktorat as identifier,
			'direktorat' as tabel
		FROM direktorat
		UNION ALL
		SELECT
			id_bagian as id,
			nama_bagian as identifier,
			'bagian' as tabel
		FROM bagian ) as target_audit
		";

		$query = $this->db->query($sql);
		return $query->result();
	}

	public function target_audit_choose($id,$table){
		$sql = "
		SELECT * FROM
		( SELECT
			id_kantor as id,
			nama_kantor as identifier,
			'kantor' as tabel
		FROM kantor
		UNION ALL
		SELECT
			id_direktorat as id,
			nama_direktorat as identifier,
			'direktorat' as tabel
		FROM direktorat
		UNION ALL
		SELECT
			id_bagian as id,
			nama_bagian as identifier,
			'bagian' as tabel
		FROM bagian ) as target_audit
		WHERE id = ".$id." AND tabel = '".$table."'
		";

		$query = $this->db->query($sql);
		return $query->row();
	}
	
	// Tambah
	public function tambah_pertanyaan($data) {
		$this->db->insert('pertanyaan',$data);
	}

	// Edit 
	public function editpertanyaan($idpertanyaan,$data) {
		$this->db->where('id_pertanyaan',$idpertanyaan);
		$this->db->update('pertanyaan',$data);
	}
	
}