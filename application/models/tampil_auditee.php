<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tampil_Auditee extends CI_Model {

	public function __construct() {
	parent::__construct();
	$this->load->database();
	}

	public function kantor() {
	$this->db->select('auditee.*, kantor.nama_kantor, direktur.nama_direktur, bagian.nama_bagian, urusan.nama_urusan');
	$this->db->from('auditee');
	$this->db->join('kantor','kantor.id_kantor = auditee.id_kantor', 'LEFT');
	$this->db->join('direktur','direktur.id_direktur = auditee.id_direktur', 'LEFT');
	$this->db->join('bagian','bagian.id_bagian = auditee.id_bagian', 'LEFT');
	$this->db->join('urusan','urusan.id_urusan = auditee.id_urusan', 'LEFT');




	$query = $this->db->get();
	return $query->result();
}